#include "DataBase.h"
#include <stdlib.h>

bool DataBase::exist(false); // init static member
string DataBase::tempPassword("");
vector<Question*> DataBase::questions(NULL);
vector<string> DataBase::usernames(NULL);
vector<int> DataBase::scores(0);
string DataBase::_status("");

DataBase::DataBase() // try opening the sqlite 
{
	int rc;
	char *zErrMsg = 0;
	
	rc = sqlite3_open("trivia.db", &_db);

	if (rc)
	{
		sqlite3_close(_db);
		string err = "Can't open database: ";
		err.append(sqlite3_errmsg(_db));
		throw exception(err.c_str());
	}
}

DataBase::~DataBase() // closing the sqlite
{
	sqlite3_close(_db);
}

bool DataBase::isUserExist(std::string username) // return true if user exist in the data base
{
	string str = "select * from t_users where username == \"" + username + "\";";
	char* zErrMsg = 0;
	int rc = sqlite3_exec(_db, str.c_str(), this->callbackCount, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		string err = "SQL error: ";
		err.append(zErrMsg);
		throw exception(err.c_str());
		return false;
	}
	return exist;
}

int DataBase::callbackCount(void* notUsed, int argc, char** argv, char** azCol) 
{
	if (argc != 0)
	{
		for (int i = 0; i < argc; i++)
		{
			if (strcmp(azCol[i], "password") == 0)
			{
				tempPassword = argv[i];
				exist = true;
				break;
			}
		}
		exist = true;
	}
	else
		exist = false;
	return 0;
}


bool DataBase::addNewUser(std::string username, std::string pass, std::string email) // adding new user
{
	string str = "insert into t_users (username, password, email) values(\"" + username + "\",\"" + pass + "\",\"" + email + "\");";

	char* zErrMsg = 0;
	int rc = sqlite3_exec(_db, str.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		string err = "SQL error: ";
		err.append(zErrMsg);
		throw exception(err.c_str());
		return false;
	}
	return true;
}

bool DataBase::isUserAndPassMatch(std::string username, std::string pass) // return true if user and pass match
{
	string str = "select password from t_users where username == \"" + username + "\";";
	char* zErrMsg = 0;
	int rc = sqlite3_exec(_db, str.c_str(), this->callbackCount, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		string err = "SQL error: ";
		err.append(zErrMsg);
		throw exception(err.c_str());
		return false;
	}
	if (pass == tempPassword)
	{
		tempPassword = "";
		return true;
	}
	tempPassword = "";
	return false;
}

int DataBase::insertNewGame() // insert new game into the game table and return the game id
{
	string str = "insert into t_games (status, start_time) values(0, DATETIME('now'));";
	char* zErrMsg = 0;
	int rc = sqlite3_exec(_db, str.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		string err = "SQL error: ";
		err.append(zErrMsg);
		throw exception(err.c_str());
		return NULL;
	}
	auto p = sqlite3_last_insert_rowid(_db);
	return p;
}

bool DataBase::updateGameStatus(int i) // update the t_games table
{
	string str1 = "update t_games set status = 1 where game_id == " + to_string(i) + ';';
	char* zErrMsg = 0;
	int rc = sqlite3_exec(_db, str1.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		string err = "SQL error: ";
		err.append(zErrMsg);
		throw exception(err.c_str());
		return false;
	}

	str1 = "update t_games set end_time = DATETIME('now') where game_id == " + to_string(i) + ';';
	zErrMsg = 0;
	rc = sqlite3_exec(_db, str1.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		string err = "SQL error: ";
		err.append(zErrMsg);
		throw exception(err.c_str());
		return false;
	}
	return false;
}

bool DataBase::addAnswerToPlayer(int gameId, std::string username, int questionId, std::string answer, bool isCorrect, int answerTime)
{ // insert new information into table t_players_answers and return if do so
	string str = "insert into t_players_answers (game_id, username, question_id, player_answer, is_correct, answer_time) values(" + to_string(gameId) + ",\"" + username + "\"," + to_string(questionId) + ",\"" + answer + "\"," + to_string(isCorrect) + "," + to_string(answerTime) + ");";
	char* zErrMsg = 0;
	int rc = sqlite3_exec(_db, str.c_str(), NULL, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		string err = "SQL error: ";
		err.append(zErrMsg);
		throw exception(err.c_str());
		return false;
	}
	return true;
}

std::vector<Question*> DataBase::initQuestions(int i)
{
	string str = "select * from t_questions;"; // take all the questions into vector member
	char*zErrMsg = 0;
	int rc = sqlite3_exec(_db, str.c_str(), this->callbackQuestions, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		string err = "SQL error: ";
		err.append(zErrMsg);
		throw exception(err.c_str());
	}
	int random;
	vector<Question*> shuffled;
	int questionSize = questions.size();

	srand(time(NULL));
	for (int j = 0; j < i; j++) // take random number position every loop
	{
		random = rand() % (questionSize - j);
		shuffled.push_back(questions[random]);
		questions.erase(questions.begin() + random);
	}
	questions.clear();
	return shuffled;
}

int DataBase::callbackQuestions(void* notUsed, int argc, char** argv, char** azCol)
{
	for (int i = 0; i < argc; i++)
	{
		if (strcmp(azCol[i], "question_id") == 0)
		{
			Question* q = new Question(stoi(argv[i]), argv[i + 1], argv[i + 2], argv[i + 3], argv[i + 4], argv[i + 5]);
			questions.push_back(q);
		}
	}
	return 0;
}

int DataBase::getQuestionsNumber()
{
	string str = "select count(*) from t_questions;"; // take all the questions into vector member
	char*zErrMsg = 0;
	int rc = sqlite3_exec(_db, str.c_str(), this->callbackStatus, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		string err = "SQL error: ";
		err.append(zErrMsg);
		throw exception(err.c_str());
	}
	return stoi(_status);
}


int DataBase::callbackUsernames(void* notUsed, int argc, char** argv, char** azCol)
{
	for (int i = 0; i < argc; i++)
	{
		if (strcmp(azCol[i], "username") == 0)
			usernames.push_back(argv[i]);
	}
	return 0;
}


std::vector<std::string> DataBase::getBestScores()
{
	string str = "select * from t_users;"; // take all the users into vector member
	char*zErrMsg = 0;
	int rc = sqlite3_exec(_db, str.c_str(), this->callbackUsernames, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		string err = "SQL error: ";
		err.append(zErrMsg);
		throw exception(err.c_str());
	}

	for (int i = 0; i < usernames.size(); i++) // push into vectors the scores of all the users
	{
		string str = "select count(*) from t_players_answers where username = \"" + usernames[i] + "\" and is_correct = 1;"; // take all the questions into vector member
		char*zErrMsg = 0;
		int rc = sqlite3_exec(_db, str.c_str(), this->callbackScores, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			string err = "SQL error: ";
			err.append(zErrMsg);
			throw exception(err.c_str());
		}
	}

	vector<string> highScores;
	int max = 0, maxj = 0;
	for (int i = 0; i < 3; i++) // pick the biggest 3 scores
	{
		for (int j = 0; j < scores.size(); j++)
		{
			if (scores[j] > max)
			{
				max = scores[j];
				maxj = j;
			}
		}
		highScores.push_back(usernames[maxj]);
		highScores.push_back(to_string(scores[maxj]));

		usernames.erase(usernames.begin() + maxj);
		scores.erase(scores.begin() + maxj);

		max = 0;
		maxj = 0;
	}
	usernames.clear();
	scores.clear();
	return highScores; 
}

std::vector<std::string> DataBase::getPersonalStatus(std::string s)
{
	vector<string> ret; //
	string str = "select count(*) from (select count(*) from t_players_answers where username = \"" + s + "\" GROUP BY game_id);";
	char* zErrMsg = 0;
	int rc = sqlite3_exec(_db, str.c_str(), callbackStatus, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		string err = "SQL error: ";
		err.append(zErrMsg);
		throw exception(err.c_str());
	}
	ret.push_back(_status);
	str = "select count(*) from t_players_answers where username = \"" + s + "\" and is_correct = 1;";
	zErrMsg = 0;
	rc = sqlite3_exec(_db, str.c_str(), callbackStatus, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		string err = "SQL error: ";
		err.append(zErrMsg);
		throw exception(err.c_str());
	}
	ret.push_back(_status);
	str = "select count(*) from t_players_answers where username = \"" + s + "\" and is_correct = 0;";
	zErrMsg = 0;
	rc = sqlite3_exec(_db, str.c_str(), callbackStatus, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		string err = "SQL error: ";
		err.append(zErrMsg);
		throw exception(err.c_str());
	}
	ret.push_back(_status);
	if (ret[0] != "0")
	{
		str = "select AVG(answer_time) from t_players_answers where username = \"" + s + "\";";
		zErrMsg = 0;
		rc = sqlite3_exec(_db, str.c_str(), callbackStatus, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			string err = "SQL error: ";
			err.append(zErrMsg);
			throw exception(err.c_str());
		}
		ret.push_back(_status);
	}
	else
		ret.push_back("0");

	return ret;

	
}

int DataBase::callbackScores(void* notUsed, int argc, char** argv, char** azCol)
{
	scores.push_back(stoi(argv[0]));
	return 0;
}


int DataBase::callbackStatus(void* notUsed, int argc, char** argv, char** azCol)
{
	_status = argv[0];
	return 0;
}


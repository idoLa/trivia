#pragma once
#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <WinSock2.h>
#include <sstream>
#include <vector>
#include <map>
#include "Question.h"
#include "sqlite3.h"

class DataBase
{
	public:
		DataBase();//
		~DataBase();//

		int getQuestionsNumber();
		bool isUserExist(std::string username);//
		bool addNewUser(std::string username, std::string pass, std::string email);//
		bool isUserAndPassMatch(std::string username, std::string pass);//
		std::vector<Question*> initQuestions(int i);
		std::vector<std::string> getBestScores();
		std::vector<std::string> getPersonalStatus(std::string s);
		int insertNewGame();//
		bool updateGameStatus(int i);//
		bool addAnswerToPlayer(int gameId, std::string username, int questionId, std::string answer, bool isCorrect, int answerTime);
	private:
		static int callbackCount(void* notUsed, int argc, char** argv, char** azCol);
		static int callbackQuestions(void* notUsed, int argc, char** argv, char** azCol);
		static int callbackScores(void*, int, char**, char**);
		static int callbackStatus(void* notUsed, int argc, char** argv, char** azCol);
		static int callbackUsernames(void* notUsed, int argc, char** argv, char** azCol);

		sqlite3* _db;
		static bool exist;
		static string tempPassword;
		static vector<Question*> questions;
		static vector<string> usernames;
		static vector<int> scores;
		static string _status;
};
#pragma once
#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>
#include <WinSock2.h>
#include <sstream>
#include <vector>
#include <map>
#include "User.h"
#include "Question.h"
#include "DataBase.h"

class User;

class Game
{
	private:
		std::vector<Question*> _questions;
		std::vector<User*> _players;
		int _questions_no;
		int _currQuestionindex;
		DataBase& _db;
		std::map<std::string, int> _results;
		int _currentTurnAnswers;
		int _game_id;

		bool insertGameToDB();
		void initQuestionsFromDB();
		void sendQuestionToAllUsers();

	public:
		Game::Game(const std::vector<User*>& players, int questionsNo, DataBase& db);
		~Game();
		void sendFirstQuestion();
		void handleFinishGame();
		bool handleNextTurn();
		bool handleAnswerFromUser(User* user, int answerNo, int time);
		bool leaveGame(User* u);
		int getID();

};
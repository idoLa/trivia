#include "RecievedMessage.h"

RecievedMessage::RecievedMessage(SOCKET sc, int i)
{
	_sock = sc;
	_messageCode = i;
}

RecievedMessage::RecievedMessage(SOCKET sc, int i, std::vector<std::string> v)
{
	_sock = sc;
	_messageCode = i;
	_values = v;
}

SOCKET RecievedMessage::getSocket()
{
	return _sock;
}

User* RecievedMessage::getUser()
{
	return _user;
}

void RecievedMessage::setUser(User* us)
{
	_user = us;
}

int RecievedMessage::getMessageCode()
{
	return _messageCode;
}

std::vector<std::string>& RecievedMessage::getValues()
{
	return _values;
}

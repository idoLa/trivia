#pragma once

#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <sstream>
#include <vector>
#include "User.h"

class RecievedMessage
{
	private:
		SOCKET _sock;
		User* _user;
		int _messageCode;
		std::vector<std::string> _values;

	public:
		RecievedMessage(SOCKET sc, int i);
		RecievedMessage(SOCKET sc, int i, std::vector<std::string> v);
		SOCKET getSocket();
		User* getUser();
		void setUser(User* us);
		int getMessageCode();
		std::vector<std::string>& getValues();

};
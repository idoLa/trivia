#pragma once

#include <vector>
#include <iostream>
#include <string>
#include "User.h"

class User;

class Room
{
	private:
		std::vector<User*> _users;
		User* _admin;
		int _maxUsers;
		int _questionTime;
		int _questionNo;
		std::string _name;
		int _id;
		//std::string getUsersAsString(std::vector<User*> usersList, User* excludeUser);
		void sendMessage(std::string str);//
		void sendMessage(User* us, std::string str);//

	public:
		Room(int id, User* ad, std::string name, int max, int qusetNo, int time);//
		bool JoinRoom(User* us);//
		void leaveRoom(User* us);//
		int closeRoom(User* us);//
		std::vector<User*> getUsers(); //
		std::string getUsersListMessage();//
		int getQuestionsNo();//
		int getId();//
		std::string getName();//
};

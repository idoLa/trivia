#include "TriviaServer.h"

#define PORT 8820
#define IFACE 0

int TriviaServer::_roomidSequence(0); // init static member

TriviaServer::TriviaServer() // try to use the function socket
{

	this->_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this->_socket == INVALID_SOCKET)
	{
		cout << WSAGetLastError() << endl;
		throw std::exception(__FUNCTION__ " - socket");
	}
		
}

TriviaServer::~TriviaServer() // close all the sockets of the trivia socket
{

	// delete rooms and users
	map<int, Room*>::iterator it;
	for (it = _roomsList.begin(); it != _roomsList.end(); it++)
	{
		delete it->second;
	}

	map<SOCKET, User*>::iterator it1;
	for (it1 = _connectedUsers.begin(); it1 != _connectedUsers.end(); it1++)
	{
		try
		{
			::closesocket(it1->first);
		}
		catch (...) {}
		delete it1->second;
	}

	

	TRACE(__FUNCTION__ " closing accepting socket");
	try
	{
		::closesocket(_socket);
	}
	catch (...) {}
}

void TriviaServer::accept() // accepting new client and start new threads
{
	SOCKET client_socket = ::accept(_socket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	TRACE("Client accepted !");
	thread ts (&TriviaServer::clientHandler, this, client_socket);
	ts.detach();
	
	
	
	
}

void TriviaServer::bindAndListen() // listen to the socket
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = IFACE;
	
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	TRACE("binded");

	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	TRACE("listening...");
}

void TriviaServer::serve() 
{
	bindAndListen();

	std::thread tr(&TriviaServer::handleRecievedMessages, this);
	tr.detach();

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		TRACE("accepting client...");
		accept();
	}
}

void TriviaServer::clientHandler(SOCKET sc) // update the messages
{
	int type;
	try
	{
		do
		{
			type = Helper::getMessageTypeCode(sc);

			RecievedMessage* rm = buildRecievedMessage(sc, type);

			addRecievedMessage(rm);

		} while (type != 0 && type != 299);
	}
	catch (exception e){
		addRecievedMessage(buildRecievedMessage(sc, 299));
	}
}

void TriviaServer::safeDeleteUser(RecievedMessage* rm) // close socket and signout
{
	handleSignOut(rm);
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		::closesocket(rm->getSocket());
	}
	catch (...) {}
}

User* TriviaServer::handleSignin(RecievedMessage* rm) // trying to sign in
{
	bool check = false;
	
	if(this->_db.isUserAndPassMatch(rm->getValues()[0], rm->getValues()[1]))
	{
		check = true;
	}
	if (check == false)
	{
		Helper::sendData(rm->getSocket(), "1021"); // wronge
		return nullptr;
	}

	if (getUserByName(rm->getValues()[0]) != nullptr)
	{
		Helper::sendData(rm->getSocket(), "1022"); // user allready exist
		return false;
	}

	User* newUS = new User(rm->getValues()[0], rm->getSocket());
	_connectedUsers.insert(pair<SOCKET, User*>(rm->getSocket(), newUS)); // add the user into the map
	Helper::sendData(rm->getSocket(), "1020");
	return newUS;
}

bool TriviaServer::handleSignUp(RecievedMessage* rm) // trying to sign up
{
	if (Vaildator::isUsernameVaild(rm->getValues()[0]) == false)
	{
		Helper::sendData(rm->getSocket(), "1043");
		TRACE("1043");
		return false;
	}

	if (Vaildator::isPasswordVaild(rm->getValues()[1]) == false)
	{
		Helper::sendData(rm->getSocket(), "1041");
		TRACE("1041");
		return false;
	}

	
	if (_db.isUserExist(rm->getValues()[0]) == true)
	{
		Helper::sendData(rm->getSocket(), "1042");
		TRACE("1042");
		return false;
	}

	if(!this->_db.addNewUser(rm->getValues()[0], rm->getValues()[1], rm->getValues()[2]))
	{
		Helper::sendData(rm->getSocket(), "1044");
		TRACE("1042");
		return false;
	}
	
	Helper::sendData(rm->getSocket(), "1040");
	TRACE("succ");
	return true;
}

void TriviaServer::handleSignOut(RecievedMessage* rm) //sign out
{
	if (rm->getUser() != nullptr)
	{
		_connectedUsers.erase(rm->getSocket());
		handleCloseRoom(rm);
		handleLeaveRoom(rm);
		//handleLeaveGame(rm);
	}
}

void TriviaServer::handleLeaveGame(RecievedMessage* rm)
{
	if (rm->getUser()->getGame()->leaveGame(rm->getUser()) == false)
	{
		rm->getUser()->getGame()->handleFinishGame();
		rm->getUser()->setGame(NULL);
	}
}

void TriviaServer::handleStartGame(RecievedMessage* rm)
{
	try
	{
		Game* g = new Game(rm->getUser()->getRoom()->getUsers(), rm->getUser()->getRoom()->getQuestionsNo(), _db);
		rm->getUser()->setGame(g);
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was catch in function: " << e.what() << std::endl;
		system("pause");
		return;
	}
	int roomId = rm->getUser()->getRoom()->getId();
	auto it = _roomsList.find(roomId);
	_roomsList.erase(it);
	rm->getUser()->clearRoom();
	rm->getUser()->getGame()->sendFirstQuestion();
}

void TriviaServer::handlePlayerAnswer(RecievedMessage* rm)
{
	if (rm->getUser()->getGame() != NULL)
	{
		if (rm->getUser()->getGame()->handleAnswerFromUser(rm->getUser(), stoi(rm->getValues()[0]) - 1, stoi(rm->getValues()[1])) == false)
		{
			rm->getUser()->getGame()->handleFinishGame();
			rm->getUser()->setGame(NULL);
		}
	}
}

bool TriviaServer::handleCreateRoom(RecievedMessage* rm) // try to create a room protocol 213
{
	if (rm->getUser() == nullptr)
		return false;
	
	int questionNo = _db.getQuestionsNumber();

	if (questionNo < stoi(rm->getValues()[2]))
	{
		rm->getUser()->send("1141");
		return false;
	}

	if (rm->getUser()->createRoom(_roomidSequence + 1, rm->getValues()[0], stoi(rm->getValues()[1]), stoi(rm->getValues()[2]), stoi(rm->getValues()[3])) == true)
	{
		_roomidSequence++;
		_roomsList.insert(pair<int, Room*>(_roomidSequence, rm->getUser()->getRoom()));
		return true;
	}
	return false;
}

bool TriviaServer::handleCloseRoom(RecievedMessage* rm) // try to close room protocol 215
{
	if (rm->getUser()->getRoom() == nullptr)
		return false;

	int id = rm->getUser()->getRoom()->getId();
	if (rm->getUser()->closeRoom() != -1)
	{
		_roomsList.erase(id);
		return true;
	}
	return false;

}

bool TriviaServer::handleJoinRoom(RecievedMessage* rm) // try to join room protocol 209
{
	if (rm->getUser() == nullptr)
		return false;

	if (getRoomByid(stoi(rm->getValues()[0])) == nullptr)
	{
		rm->getUser()->send("1102");
		return false;
	}

	if (rm->getUser()->joinRoom(getRoomByid(stoi(rm->getValues()[0]))) != true)
		return false;

	return true;


}

bool TriviaServer::handleLeaveRoom(RecievedMessage* rm) // try to leave a room protocol 211
{
	if (rm->getUser() == nullptr)
		return false;

	if (rm->getUser()->getRoom() == nullptr)
		return false;

	rm->getUser()->leaveRoom();
	return true;
}

void TriviaServer::handleGetUsersInRoom(RecievedMessage* rm)// protocol 207 get users from room
{
	if (getRoomByid(stoi(rm->getValues()[0])) == nullptr)
		rm->getUser()->send("1080");

	else
		rm->getUser()->send(getRoomByid(stoi(rm->getValues()[0]))->getUsersListMessage());
	
}

void TriviaServer::handleGetRooms(RecievedMessage* rm)// protocol 205 get the rooms
{
	string message = "106";
	message = message + Helper::getPaddedNumber(_roomsList.size(), 4);

	map<int, Room*>::iterator it;
	for (it = _roomsList.begin(); it != _roomsList.end(); it++)
	{
		message = message + Helper::getPaddedNumber(it->first, 4);
		message = message + Helper::getPaddedNumber(it->second->getName().size(), 2);
		message = message + it->second->getName();
	}

	rm->getUser()->send(message);
}

void TriviaServer::handleGetBestScores(RecievedMessage* rm) // 223
{
	vector<string> highScores = _db.getBestScores();
	string message = "124";
	for (int i = 0; i < highScores.size(); i = i + 2)
	{
		message = message + Helper::getPaddedNumber(highScores[i].size(), 2);
		message = message + highScores[i];
		message = message + Helper::getPaddedNumber(stoi(highScores[i + 1]), 6);
	}
	rm->getUser()->send(message);
}

void TriviaServer::handleGetPersonalStatus(RecievedMessage* rm)
{
	vector<string> status = this->_db.getPersonalStatus(rm->getUser()->getUsername());
	int dot_pos;
	string time = "";
	for (int i = 0; i < status[3].size(); i++)
	{
		if (status[3][i] == '.')
			dot_pos = i;
	}

	if (status[3] != "0")
	{
		if (dot_pos == 1)
			time = time + "0" + status[3][dot_pos - 1] + status[3][dot_pos + 1] + status[3][dot_pos + 2];

		else
			time = status[3][dot_pos - 2] + status[3][dot_pos - 1] + status[3][dot_pos + 1] + status[3][dot_pos + 2];
	}
	else
		time = "0000";


	string str = "126" + Helper::getPaddedNumber(stoi(status[0]), 4) + Helper::getPaddedNumber(stoi(status[1]), 6) + Helper::getPaddedNumber(stoi(status[2]), 6) + time;
	cout << status[3] << endl;
	rm->getUser()->send(str);
}

void TriviaServer::handleRecievedMessages()  // call the right function by the protocol
{
	std::unique_lock<std::mutex> lck(_mtxRecievedMessages, defer_lock);
	RecievedMessage* rm;

	do 
	{
		
		lck.lock();
		cv.wait(lck);



		rm = _queRcvMessages.front();
		_queRcvMessages.pop();

		lck.unlock();


		rm->setUser(getUsersBySocket(rm->getSocket())); /// ask itay

		if (rm->getMessageCode() == 200)
			rm->setUser(handleSignin(rm)); // not sure

		else if (rm->getMessageCode() == 201)
			handleSignOut(rm);

		else if (rm->getMessageCode() == 203)
			bool suc = handleSignUp(rm); // not sure

		else if (rm->getMessageCode() == 205)
			handleGetRooms(rm);

		else if (rm->getMessageCode() == 207)
			handleGetUsersInRoom(rm);

		else if (rm->getMessageCode() == 209)
			bool suc = handleJoinRoom(rm);

		else if (rm->getMessageCode() == 211)
			bool suc = handleLeaveRoom(rm);

		else if (rm->getMessageCode() == 213)
		{
			if (handleCreateRoom(rm) == false)
			{
				Helper::sendData(rm->getSocket(), "1141");
			}
		}

		else if (rm->getMessageCode() == 215)
			bool suc = handleCloseRoom(rm);

		else if (rm->getMessageCode() == 217)
			handleStartGame(rm);

		else if (rm->getMessageCode() == 219)
			handlePlayerAnswer(rm);

		else if (rm->getMessageCode() == 222)
			handleLeaveGame(rm);

		else if (rm->getMessageCode() == 223)
			handleGetBestScores(rm);

		else if (rm->getMessageCode() == 225)
			handleGetPersonalStatus(rm);

		/*else if (rm->getMessageCode() == 299)
			safeDeleteUser(rm);

		else
			safeDeleteUser(rm);*/
	} while (rm->getMessageCode() != 299);
	safeDeleteUser(rm);

}

void TriviaServer::addRecievedMessage(RecievedMessage* rm) // add new message to the vector
{
	std::lock_guard<std::mutex> lock(_mtxRecievedMessages);
	_queRcvMessages.push(rm);
	cv.notify_one(); // wait until the cv wakes up
}

RecievedMessage* TriviaServer::buildRecievedMessage(SOCKET sc, int i) 
{ // get socket and protocol, building message and return RecievedMessage object
	vector<std::string> v;
	
	

	bool vectorCount = false;
	if (i == 200)
	{
		int userLen = Helper::getIntPartFromSocket(sc, 2);
		string userName = Helper::getStringPartFromSocket(sc, userLen);
		int passLen = Helper::getIntPartFromSocket(sc, 2);
		string password = Helper::getStringPartFromSocket(sc, passLen);

		v.push_back(userName);
		v.push_back(password);

		vectorCount = true;
	}

	else if (i == 203)
	{
		int userLen = Helper::getIntPartFromSocket(sc, 2);
		string userName = Helper::getStringPartFromSocket(sc, userLen);
		int passLen = Helper::getIntPartFromSocket(sc, 2);
		string password = Helper::getStringPartFromSocket(sc, passLen);
		int emailLen = Helper::getIntPartFromSocket(sc, 2);
		string email = Helper::getStringPartFromSocket(sc, emailLen);

		v.push_back(userName);
		v.push_back(password);
		v.push_back(email);

		vectorCount = true;
	}

	else if (i == 207)
	{
		int id = Helper::getIntPartFromSocket(sc, 4);
		

		v.push_back(to_string(id));

		vectorCount = true;
	}

	else if (i == 209)
	{
		int id = Helper::getIntPartFromSocket(sc, 4);


		v.push_back(to_string(id));

		vectorCount = true;
	}

	else if (i == 213)
	{
		int nameLen = Helper::getIntPartFromSocket(sc, 2);
		string roomName = Helper::getStringPartFromSocket(sc, nameLen);

		int playersNumber = Helper::getIntPartFromSocket(sc, 1);

		int questionsNumber = Helper::getIntPartFromSocket(sc, 2);
		int questionTimeInSec = Helper::getIntPartFromSocket(sc, 2);

		v.push_back(roomName);
		v.push_back(to_string(playersNumber));
		v.push_back(to_string(questionsNumber));
		v.push_back(to_string(questionTimeInSec));

		vectorCount = true;
	}

	else if (i == 219)
	{
		int answerNumber = Helper::getIntPartFromSocket(sc, 1);
		int timeInSeconds = Helper::getIntPartFromSocket(sc, 2);

		v.push_back(to_string(answerNumber));
		v.push_back(to_string(timeInSeconds));

		vectorCount = true;
	}

	if (vectorCount == true)
	{
		RecievedMessage* newRM = new RecievedMessage(sc, i, v);
		return newRM;
	}
	else
	{
		RecievedMessage* newRM = new RecievedMessage(sc, i);
		return newRM;
	}
}

///////
User* TriviaServer::getUserByName(std::string name) // get user by name
{
	map<SOCKET, User*>::iterator it;
	for (it = _connectedUsers.begin(); it != _connectedUsers.end(); it++)
	{
		if (name == it->second->getUsername())
			return it->second;
	}
	return nullptr;
}

User* TriviaServer::getUsersBySocket(SOCKET sc) // get user by socket
{
	auto newUS = _connectedUsers.find(sc);
	if (newUS == _connectedUsers.end())
		return nullptr;

	return _connectedUsers.find(sc)->second;
}

Room* TriviaServer::getRoomByid(int i) // get room by id
{
	return _roomsList.find(i)->second;
}


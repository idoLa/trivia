#include "User.h"

User::User(std::string s1, SOCKET s)
{
	_username = s1;
	_currRoom = nullptr;
	_currGame = nullptr;
	_sock = s;
}

void User::setGame(Game* g)
{
	_currGame = g;
}

void User::clearRoom()
{
	_currRoom = nullptr;
}

bool User::createRoom(int roomId, std::string roomName, int maxUsers, int questionsNo, int questionTime)
{								// if the current user is already in room, can't create room
	if (_currRoom != nullptr) // if he allready in room
	{
		send("1141");
		return false;
	}

	//else creates new room and send the right massege.
	_currRoom = new Room(roomId, this, roomName, maxUsers, questionsNo, questionTime);
	send("1140"); 
	return true;
}



bool User::joinRoom(Room* r)
{						// try to join a room
	if (_currRoom != nullptr)//if the curr user already has a room, cant join room.
		return false;

	if (r->JoinRoom(this) == true)//if the curr user does not get any room, joins room.
	{
		this->_currRoom = r;
		return true;
	}

	return false;
}


void User::leaveRoom()
{
	if (_currRoom != nullptr)//if the user is in room, the room is deleted.
	{
		_currRoom->leaveRoom(this);
		_currRoom = nullptr;
	}

}

int User::closeRoom()
{
	if (_currRoom != nullptr)//if the user is in room, the room is closed.
	{
		if (_currRoom->closeRoom(this) != -1)//if the close room succseed, deletes the room.
		{
			int id = _currRoom->getId();
			delete _currRoom;
			_currRoom = nullptr;
			return id;
		}
	}

	return -1;
}



void User::send(std::string mess)
{
	Helper::sendData(_sock, mess);
}

string User::getUsername()
{
	return _username;
}

SOCKET User::getSocket()
{
	return _sock;
}

Room* User::getRoom()
{
	return _currRoom;
}

Game* User::getGame()
{
	return _currGame;
}

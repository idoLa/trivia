#pragma once
#include <iostream>
#include <string.h>
#include <sys/types.h>
#include <sstream>
#include <vector>
#include "Room.h"
#include "Game.h"

class Vaildator
{
	public:
		static bool isPasswordVaild(std::string);
		static bool isUsernameVaild(std::string);
};